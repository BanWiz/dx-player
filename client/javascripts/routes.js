'use strict';

module.exports = function ($routeProvider) {
    $routeProvider
        .when('/login/:returnURI',
        {
            controller: 'AuthController',
            templateUrl: 'auth.html'
        })

        .when('/captcha/:returnURI',
        {
            controller: 'CaptchaController',
            templateUrl: 'captcha.html'
        })

        .when('/user',
        {
            controller: 'UserController',
            templateUrl: 'user.html'
        })
        .when('/search',
        {
            controller: 'SearchController',
            templateUrl: 'search.html',
            reloadOnSearch: false
        })
        .when('/now-playing',
        {
            controller: 'NowPlayingController',
            templateUrl: 'now.html'
        })
        .otherwise({ redirectTo: '/user' });
};