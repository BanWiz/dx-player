'use strict';

var CaptchaVal = require('./srvcs/CaptchaValue'),
    Player = require('./srvcs/PlayerService'),
    Utils = require('./srvcs/Utils'),
    Proxy = require('./srvcs/ProxyService'),
    VkAuth = require('./srvcs/VkAuthService'),
    VkAudio = require('./srvcs/VkAudioService');

module.exports = function (app) {
    app.value('captchaValue', CaptchaVal);
    app.factory('playerService', Player);
    app.factory('proxyService', Proxy);
    app.factory('utils', ['$location', '$sce', 'captchaValue', Utils ]);
    app.factory('vkAudioService', ['$q', 'vkAuthService', 'captchaValue', VkAudio ]);
    app.factory('vkAuthService', ['$q', VkAuth ]);
};