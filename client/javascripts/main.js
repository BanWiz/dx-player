"use strict";

require('angular');
require('angular-route');
require('angular-animate');
require('ng-infinite-scroller/build/ng-infinite-scroll');
require('./templates');

var app = angular.module('audioApp', ['templates', 'ngRoute', 'ngAnimate', 'infinite-scroll']);

var routes = require('./routes');

app.config(['$routeProvider', routes ]);

require('./directives')(app);
require('./services')(app);
require('./controllers')(app);