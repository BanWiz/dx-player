'use strict';

var NavbarCtrl = require('./ctrls/NavbarController'),
    AuthCtrl = require('./ctrls/AuthController'),
    CaptchaCtrl = require('./ctrls/CaptchaController'),
    PlayerCtrl = require('./ctrls/PlayerController'),
    SearchCtrl = require('./ctrls/SearchController'),
    UserCtrl = require('./ctrls/UserController'),
    NowPlayingCtrl = require('./ctrls/NowPlayingController');

module.exports = function (app) {

    app.controller('AuthController', ['$scope', 'utils', '$routeParams', 'vkAuthService', AuthCtrl ]);
    app.controller('CaptchaController', ['$scope', 'utils', '$routeParams', 'proxyService', 'captchaValue', CaptchaCtrl ]);
    app.controller('NavbarController', ['$scope', '$location', NavbarCtrl ]);
    app.controller('PlayerController', ['$scope', 'utils', 'proxyService', 'playerService', 'vkAudioService', PlayerCtrl ]);
    app.controller('SearchController', ['$scope', 'utils', '$routeParams', 'vkAudioService', 'playerService', SearchCtrl ]);
    app.controller('UserController', ['$scope', 'utils', 'vkAudioService', 'playerService', UserCtrl ]);
    app.controller('NowPlayingController', ['$scope', 'utils', 'playerService', NowPlayingCtrl ]);
};