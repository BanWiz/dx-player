"use strict";

module.exports = function ($q, vkAuthService, captchaValue) {

    var loadLyrics = function (lyricsId) {
        var deferred = $q.defer();
        var token = vkAuthService.getToken();
        if (token == null) {
            deferred.reject({ error_msg: 'Access token expired', error_code: 5 });
        } else {
            $.post('/lyrics', { lyrics_id: lyricsId, token: token })
                .done(function (result) {
                    if (result.error) {
                        deferred.reject(result.error);
                    }
                    deferred.resolve(result.response.text);
                })
                .fail(function (jqxhr) {
                    deferred.reject({ error_msg: jqxhr.statusText, error_code: jqxhr.status });
                });
        }
        return deferred.promise;
    };

    var loadUserAudios = function () {
        var deferred = $q.defer();
        var token = vkAuthService.getToken();
        if (token == null) {
            deferred.reject({ error_msg: 'Access token expired', error_code: 5 })
        } else {
            var requestData = {
                token: token
            };
            if (captchaValue.captchaSid && captchaValue.captchaKey) {
                requestData.captcha = 'yes';
                requestData.captcha_sid = captchaValue.captchaSid;
                requestData.captcha_key = captchaValue.captchaKey;
            }
            $.post('/audio', requestData)
                .done(function (data) {
                    captchaValue.captchaKey = captchaValue.captchaSid = captchaValue.captchaUrl = null;
                    if (data.error) {
                        console.log(data.error);
                        deferred.reject(data.error);
                        return;
                    }
                    deferred.resolve(data.response);
                })
                .fail(function (jqxhr) {
                    deferred.reject({ error_msg: jqxhr.statusText, error_code: jqxhr.status });
                });
        }

        return deferred.promise;
    };

    var search = function (query) {
        var deferred = $q.defer();
        var token = vkAuthService.getToken();
        if (token == null) {
            deferred.reject({ error_msg: 'Access token expired', error_code: 5 })
        } else {
            var requestData = {
                token: token,
                query: query
            };
            if (captchaValue.captchaSid && captchaValue.captchaKey) {
                requestData.captcha = 'yes';
                requestData.captcha_sid = captchaValue.captchaSid;
                requestData.captcha_key = captchaValue.captchaKey;
            }
            $.post('/vk-search', requestData)
                .done(function (data) {
                    captchaValue.captchaKey = captchaValue.captchaSid = captchaValue.captchaUrl = null;
                    if (data.error) {
                        deferred.reject(data.error);
                        return;
                    }
                    var results = data.response;
                    if (!(results[0] instanceof Object)) {
                        results.shift();
                    }
                    deferred.resolve(results);
                })
                .fail(function (jqxhr) {
                    deferred.reject({ error_msg: jqxhr.statusText, error_code: jqxhr.status });
                });
        }
        return deferred.promise;
    };

    var addToUserAudios = function (audio) {
        var deferred = $q.defer();
        var token = vkAuthService.getToken();
        if (token == null) {
            deferred.reject({ error_msg: 'Access token expired', error_code: 5 })
        } else {
            var requestData = {
                token: token,
                aid: audio.aid,
                oid: audio.owner_id
            };

            $.post('/add', requestData)
                .done(function (data) {
                    if (data.error) {
                        deferred.reject(data.error);
                        return;
                    }
                    deferred.resolve();
                })
                .fail(function (jqxhr) {
                    deferred.reject({ error_msg: jqxhr.statusText, error_code: jqxhr.status });
                });
        }
        return deferred.promise;
    };

    var removeFromUserAudios = function(audio) {
        var deferred = $q.defer();
        var token = vkAuthService.getToken();
        if (token == null) {
            deferred.reject({ error_msg: 'Access token expired', error_code: 5 })
        } else {
            var requestData = {
                token: token,
                aid: audio.aid,
                oid: audio.owner_id
            };

            $.post('/remove', requestData)
                .done(function (data) {
                    if (data.error) {
                        deferred.reject(data.error);
                        return;
                    }
                    deferred.resolve();
                })
                .fail(function (jqxhr) {
                    deferred.reject({ error_msg: jqxhr.statusText, error_code: jqxhr.status });
                });
        }
        return deferred.promise;
    };

    return {
        loadLyrics: loadLyrics,
        loadUserAudios: loadUserAudios,
        search: search,
        addToUserAudios: addToUserAudios,
        removeFromUserAudios: removeFromUserAudios
    };

};