"use strict";

module.exports = function () {
    var player = {
        current: {},
        playlist: [],
        index: null
    };

    var setCurrent = function (index, audio, playlist) {
        if (playlist) {
            player.playlist = playlist;
        }
        var next;
        if (audio) {
            next = audio;
        } else {
            next = player.playlist[index];
        }
        if (next) {
            player.index = index;
            player.current = next;
        }
    };
    var getCurrent = function () {
        return player;
    };

    var shufflePlaylist = function () {
        player.playlist.splice(player.index, 1);
        var randomComparer = function () {
            return Math.random() - 0.5;
        };
        player.playlist.sort(randomComparer);
        player.playlist.unshift(player.current);
        player.index = 0;
        for(var i = 0; i < shufflePlaylist.subscribers.length; i++) {
            shufflePlaylist.subscribers[i](player.playlist);
        }
    };

    shufflePlaylist.subscribers = [];
    shufflePlaylist.subscribe = function(subscribeFunction) {
        shufflePlaylist.subscribers.push(subscribeFunction);
    };

    return {
        getCurrent: getCurrent,
        setCurrent: setCurrent,
        shufflePlaylist: shufflePlaylist
    };
};