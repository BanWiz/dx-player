'use strict';

module.exports = function ($location, $sce, captchaValue) {
    var redirectTo = function (route, addReturnUri) {
        if (addReturnUri !== false) {
            var currentLocation = $location.path();
            var encodedLocation = encodeURIComponent(currentLocation);
            $location.path(route + '/' + encodedLocation);
        } else {
            $location.path(route);
        }
    };

    var addSearchQuery = function(queryString) {
        $location.search('query', queryString);
    };

    var handleVkError = function(error) {
        if (error.error_code == 14) {
            captchaValue.captchaSid = error.captcha_sid;
            captchaValue.captchaUrl = error.captcha_img;
            redirectTo('/captcha');
            return;
        }
        if (error.error_code == 5) {
            redirectTo('/login');
            return;
        }
        alert(error.error_msg);
    };

    var safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    var trustHtml = function (html) {
        return $sce.trustAsHtml(html);
    };

    var formatTime = function (fullSeconds) {
        var seconds = fullSeconds % 60;
        var minutes = (fullSeconds - seconds) / 60;
        var secondsString = seconds.toString();
        if (secondsString.length == 1) {
            secondsString = "0" + secondsString;
        }
        return minutes + ":" + secondsString;
    };

    return {
        safeApply: safeApply,
        trustHtml: trustHtml,
        formatTime: formatTime,
        redirectTo: redirectTo,
        addSearchQuery: addSearchQuery,
        handleVkError: handleVkError
    }
};