'use strict';

module.exports = function () {
    return function (scope, element, attr) {
        var callback = attr['vaEnded'];
        element.on('ended', function () {
            scope.safeApply(scope.$eval(callback));
        });
    };
};