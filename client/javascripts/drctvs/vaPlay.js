'use strict';

module.exports = function () {
    return function (scope, element, attr) {
        var audioSelector = attr['vaPlay'];
        var audio = document.querySelector(audioSelector);
        if (audio == null) {
            throw "Audio element not found";
        }
        var iconSpan = element.children('span')[0];

        element.on('click', function () {
            if (audio.paused) {
                audio.play();
                return;
            }
            audio.pause();
        });
        audio.addEventListener('pause', function () {
            iconSpan.classList.remove('glyphicon-pause');
            iconSpan.classList.add('glyphicon-play');
        });
        audio.addEventListener('play', function () {

            iconSpan.classList.remove('glyphicon-play');
            iconSpan.classList.add('glyphicon-pause');
        });
    }
};