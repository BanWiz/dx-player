'use strict';

module.exports = function () {
    return function (scope, element, attr) {
        var audioSelector = attr['vaVolume'],
            audio = document.querySelector(audioSelector),
            volumeControl = element[0];

        if (audio == null) {
            throw "Audio element not found";
        }
        volumeControl.min = 0;
        volumeControl.max = 100;
        volumeControl.step = 1;
        audio.volume = 0.5;
        volumeControl.value = 50;

        element.on('change', function () {
            audio.volume = volumeControl.value / 100;
        });
    }
};