'use strict';

module.exports = function () {
    return function (scope, element, attr) {
        var audioSelector = attr['vaProgressFor'];
        var audioTag = document.querySelector(audioSelector);
        if (audioTag == null) {
            throw "Audio element not found";
        }
        var progressElem = element[0];

        element.on('click', function (e) {
            var position = e.clientX;
            var leftOffset = progressElem.offsetWidth;
            var audioDuration = audioTag.duration;
            audio.currentTime = position * audioDuration / leftOffset;
        });

        audioTag.addEventListener('timeupdate', function () {
            var audio = this;
            var duration = audio.duration;
            var currentTime = audio.currentTime;
            if (!duration || !currentTime) {
                return;
            }
            progressElem.value = currentTime / duration;
        });
    }
};