'use strict';

module.exports = function () {
    var $window = $(window),
        lastPosition = 0;
    return function (scope, element) {
        element.on('click', function () {
            var currentPosition = $window.scrollTop();
            if (currentPosition === 0) {
                $window.scrollTop(lastPosition);
            } else {
                $window.scrollTop(0);
                lastPosition = currentPosition;
            }
        })
    }
};