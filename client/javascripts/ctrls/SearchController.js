"use strict";


module.exports = function ($scope, utils, $routeParams, vkAudioService, playerService) {
    $scope.safeApply = utils.safeApply;
    $scope.trust = utils.trustHtml;

    $scope.query = '';

    $scope.loading = false;

    var results = [];
    $scope.visibleAudios = [];
    var infiniteAdd = function (data, target) {
        var firstIndexToAdd = target.length;
        for (var i = 0; i < 30; i++) {
            var current = data[firstIndexToAdd + i];
            if (current) {
                target.push(current);
            }
        }
    };
    $scope.loadMore = function () {
        infiniteAdd(results, $scope.visibleAudios);
    };

    var loadSearchResults = function (queryString) {
        $scope.loading = true;
        vkAudioService.search(queryString).then(
            function (res) {
                $scope.safeApply(function () {
                    results = res;
                    $scope.visibleAudios = results.slice(0, 80);
                    $scope.loading = false;
                });
                localStorage.setItem('vk-search', queryString);
            },
            function (error) {
                utils.handleVkError(error);
            }
        )
    };

    $scope.find = function ($event) {
        $event.preventDefault();
        if ($scope.query) {
            utils.addSearchQuery($scope.query);
            loadSearchResults($scope.query);
        }
    };


    $scope.player = playerService.getCurrent();

    $scope.playAudio = function (audio, index) {
        $scope.safeApply(function () {
            playerService.setCurrent(index, audio, results.slice(0, results.length - 1));
        });
    };

    $scope.addToUserAudios = function (audio, $event) {
        $event.stopPropagation();
        vkAudioService.addToUserAudios(audio).then(
            function () {
                $scope.safeApply(function () {
                    audio.saved = true;
                })
            },
            function (error) {
                utils.handleVkError(error);
            }
        );
    };

    $scope.formatTime = utils.formatTime;

    if ($routeParams.query) {
        $scope.query = $routeParams.query;
        loadSearchResults($routeParams.query);
    } else {
        var storedQuery = localStorage.getItem('vk-search');
        if (storedQuery) {
            $scope.query = storedQuery;
            utils.addSearchQuery(storedQuery);
            loadSearchResults(storedQuery);
        }
    }
};
