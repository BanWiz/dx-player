"use strict";

module.exports = function ($scope, utils, playerService) {
    $scope.safeApply = utils.safeApply;
    $scope.trust = utils.trustHtml;

    $scope.player = playerService.getCurrent();
    $scope.visibleAudios = $scope.player.playlist.slice(0, 80);

    $scope.playAudio = function (audio, index) {
        $scope.safeApply(function () {
            playerService.setCurrent(index, audio);
        });
    };

    $scope.formatTime = utils.formatTime;

    var infiniteAdd = function (data, target) {
        var firstIndexToAdd = target.length;
        for (var i = 0; i < 30; i++) {
            var current = data[firstIndexToAdd + i];
            if (current) {
                target.push(current);
            }
        }
    };

    $scope.loadMore = function () {
        infiniteAdd($scope.player.playlist, $scope.visibleAudios);
    };

    playerService.shufflePlaylist.subscribe(function (shuffledList) {
        $scope.safeApply(function() {
            $scope.visibleAudios = shuffledList.slice(0, 80);
        })
    });
};