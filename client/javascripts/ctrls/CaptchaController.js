"use strict";


module.exports = function ($scope, utils, $routeParams, proxyService, captchaValue) {
    $scope.safeApply = utils.safeApply;

    var returnUri = $routeParams.returnURI;
    var captchaUrl = captchaValue.captchaUrl;
    var captchaSid = captchaValue.captchaSid;
    if (!captchaUrl || !captchaSid) {
        utils.redirectTo(returnUri, false);
        return;
    }

    $scope.proxy = proxyService.createProxy;
    $scope.captchaUrl = captchaUrl;
    $scope.captchaKey = '';

    $scope.saveEnteredCaptcha = function ($event) {
        $event.preventDefault();
        captchaValue.captchaKey = $scope.captchaKey;
        utils.redirectTo(returnUri, false);
    }
};