"use strict";

var
    express = require('express'),
    routes = require('./routes'),
    http = require('http'),
    path = require('path'),
    config = require('./config');

var app = express();

// all environments
app.use(express.favicon(path.join(__dirname, 'client/favicon.ico')));
app.use(express.logger('default'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.errorHandler());

// pages
routes(app);

http.createServer(app).listen(config.get('port'), process.env.IP, function () {
    console.log('Express Server is listening on ' + config.get('port'));
});
