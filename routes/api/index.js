'use strict';

var vk = require('./vk'),
    proxy = require('./proxy');

module.exports = function(app) {
    app.post('/login', vk.login);
    app.post('/audio', vk.audio);
    app.post('/lyrics', vk.lyrics);
    app.post('/vk-search', vk.search);
    app.post('/add', vk.add);
    app.post('/remove', vk.remove);
    app.get('/proxy', proxy);
};