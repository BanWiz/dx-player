"use strict";

var
    config = require('../../config'),
    vkAuth = require('vk-auth')(config.get('vk:app'), config.get('vk:permissions')),
    vkontakte = require('../../lib/vkontakte');

var login = function (req, res) {
    var login = req.body.login;
    var password = req.body.password;
    vkAuth.authorize(login, password);
    vkAuth.once('error', function(err) {
        res.json({
            error: {
                error_code: 99,
                error_msg: err.message
            }
        })
    });
    vkAuth.once('auth', function(tokenParams) {
        res.json({
            response: tokenParams
        })
    })
};

var audio = function (req, res) {
    var token = req.body.token;
    var vk = vkontakte(token);
    var options = {};
    if (req.body.captcha == "yes") {
        options.captcha_sid = req.body.captcha_sid;
        options.captcha_key = req.body.captcha_key;
    }

    vk('audio.get', options).pipe(res);
};

var lyrics = function (req, res) {
    var token = req.body.token;
    var lyricsId = req.body.lyrics_id;
    var vk = vkontakte(token);

    vk('audio.getLyrics', { lyrics_id: lyricsId }).pipe(res);

};

var search = function (req, res) {
    var token = req.body.token;
    var query = req.body.query;
    var vk = vkontakte(token);
    var options = {
        q: query,
        auto_complete: 0,
        sort: 2,
        count: 300
    };
    if (req.query.captcha == "yes") {
        options.captcha_sid = req.query.captcha_sid;
        options.captcha_key = req.query.captcha_key;
    }

    vk('audio.search', options).pipe(res);
};

var add = function (req, res) {
    var token = req.body.token;
    var aid = req.body.aid;
    var oid = req.body.oid;
    var vk = vkontakte(token);
    var options = {
        aid: aid,
        oid: oid
    };

    vk('audio.add', options).pipe(res);
};

var remove = function(req, res) {
    var token = req.body.token,
        aid = req.body.aid,
        oid = req.body.oid,
        vk = vkontakte(token),
        options = {
            audio_id: aid,
            owner_id: oid
        };

    vk('audio.delete', options).pipe(res);
};

exports.login = login;
exports.audio = audio;
exports.lyrics = lyrics;
exports.search = search;
exports.add = add;
exports.remove = remove;