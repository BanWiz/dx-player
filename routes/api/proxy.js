var http = require('http');
var urlParser = require('url');

var proxy = function(req, res) {
    var url = req.query.url;
    req.pause();
    var parsedUrl = urlParser.parse(url);
    var options = {
        host: parsedUrl.host,
        path: parsedUrl.path,
        method: req.method
    }
    var connector = http.request(options, function(serverResponse) {
        serverResponse.pause();
        res.writeHeader(serverResponse.statusCode, serverResponse.headers);
        serverResponse.pipe(res);
        serverResponse.resume();
    });

    res.on('close', function() {
        connector.destroy();
    })
    req.pipe(connector);
    req.resume();
}

module.exports = proxy;

