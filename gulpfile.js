"use strict";
// Load plugins
var gulp = require('gulp'),
    $ = require('gulp-load-plugins')({camelize: true});

// Styles
gulp.task('styles', function () {
    return gulp.src(['client/libs/bootstrap/dist/css/bootstrap.css', 'client/stylesheets/style.css'])
        .pipe($.autoprefixer('last 2 version'))
        .pipe($.concat('styles.css'))
        .pipe($.minifyCss())
        .pipe(gulp.dest('dist/client/css'))
});

// Pages
gulp.task('pages', function () {
    return gulp.src('client/pages/*.jade')
        .pipe($.jade({
            pretty: true
        }))
        .pipe(gulp.dest('dist/client'))
});

// Precompile angular templates
gulp.task('templates', function () {
    return gulp.src('client/templates/*.jade')
        .pipe($.jade({
            pretty: true
        }))
        .pipe($.angularTemplatecache({
            standalone: true
        }))
        .pipe(gulp.dest('client/javascripts'))
});

gulp.task('scripts', ['templates'], function () {
    return gulp.src('client/javascripts/index.js')
        .pipe($.browserify({
            transform: ['debowerify']
        }))
        .pipe($.uglify())
        .pipe($.rename('scripts.js'))
        .pipe(gulp.dest('./dist/client/js'))
});

// Clean
gulp.task('clean', function () {
    return gulp.src(['dist/*', '!dist/node_modules'], {read: false})
        .pipe($.clean())
});

gulp.task('copy', function () {
    return gulp.src([
        './**/*',
        '!client/**/*',
        'client/favicon.ico',
        '!dist',
        '!dist/**/*',
        '!node_modules/**/*',
        '!.bowerrc',
        '!.gitignore',
        '!bower.json',
        '!gulpfile.js'
    ], { base: './' })
        .pipe(gulp.dest('dist'))
});

gulp.task('copyFonts', function () {
    return gulp.src(['client/libs/bootstrap/dist/fonts/*'])
        .pipe(gulp.dest('dist/client/fonts'));
});

// Default task
gulp.task('default', ['clean'], function () {
    gulp.start('scripts');
    gulp.start('styles');
    gulp.start('pages');
    gulp.start('copyFonts');
    gulp.start('copy');
});

gulp.task('watch', function() {
    var server = $.livereload();
    gulp.watch('client/stylesheets/**/*', ['styles'])
    gulp.watch(['client/javascripts/**/*', 'client/templates/**/*'], ['scripts']);
    gulp.watch('client/pages/**/*', ['pages']);
    gulp.watch('dist/**').on('change', function(file) {
        server.changed(file.path);
    });
});