# **DX Player**

DX Player is an application for listening music from VK social network that provides user-friendly interface and access to your music from places where VK is not accessible by proxying requests through server.

## Core features
* Using remote server for interaction with VK. All requests from client are sent to server, preprocessed and then redirected to target
* Displaying of user's audio list
* Search through VK music library
* Adding audios from search to user's audio list
* Storing of currently playing list while navigating over player's pages
* Displaying of song lyrics if they are available

## Сhangelog
*31.03.2014*

* Animations changed to make switching between pages more smooth

*21.03.2014*

* Switched to light theme
* Fixed loading animation in FF and IE11

*13.03.2014*

* Improved app pages loading speed
* Improved app responsiveness
* Decreased amount of requests to server for loading resources
* Added build system (Gulp)

*11.03.2014*

* Volume control added
* Number of audios in search results increased to 300
* Ability to remove audios from user's list


*10.03.2014*

* Button for quick scrolling to page top
* Ok icon is displayed when audio from search results successfully added to user's list
* Server restarting when incorrect credentials are entered fixed

*03.03.2014*

* Exception handling for expired token on lyrics request
* Loading of last search query when returning back to search page

*02.03.2014*

* Config for server created
* Some server requests changed from GET to POST

*01.03.2014*

* README file added
* Changelog started
* Authorization speed improved by twice
* Ability to use phone number as login
* Error handling improvements for authorization
* Play/Pause button now changes it's icon

## Credits
Feel free to open issue in case you have any questions/ideas for improving or found some bugs in already implemented functionality.
Also you can [contact me through e-mail](mailto:darkxahtep@gmail.com).
#### License: MIT

###_Сontributions and bug reporting are welcomed!_